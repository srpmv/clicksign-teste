import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';

import { NoContactComponent } from './no-contact/no-contact.component';
import { ContactsComponent } from './contacts/contacts.component';

@NgModule({
  declarations: [
    NoContactComponent,
    ContactsComponent
  ],
  imports:[
    FormsModule, BrowserModule
  ],
  exports: [
    NoContactComponent,
    ContactsComponent
  ]
})
export class ComponentsModule { }
