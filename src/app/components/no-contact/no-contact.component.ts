import { Component, OnInit, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-no-contact',
  templateUrl: './no-contact.component.html',
  styleUrls: ['./no-contact.component.scss']
})
export class NoContactComponent implements OnInit {
  @Output() emitContact : EventEmitter <object> = new EventEmitter<object>();

  public contact = {
    name: '',
    email: '',
    telephone: ''
  }
 
  constructor() { }

  ngOnInit() { }

  user() {
    this.emitContact.emit(this.contact);
  }

}
