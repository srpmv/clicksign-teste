import { Component } from '@angular/core';
declare var $: any;
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  public contact = {
    name: '',
    email: '',
    telephone: ''
  }
  public contacts = [];
  constructor() { }

  create() {
    this.contacts.push(this.contact);
    this.contact = {
      name: '',
      email: '',
      telephone: ''
    }
    $('#modal').modal('hide');
  }
  addContact(e) {
    console.log(e);
    this.contacts.push(e);
  }
}